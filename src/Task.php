<?php
namespace EasyTask;

use EasyTask\Helper\ProcessHelper;
use EasyTask\Helper\UtilHelper;
use EasyTask\Process\Linux;
use EasyTask\Process\Win;
use \Closure as Closure;
use \ReflectionClass as ReflectionClass;
use \ReflectionException as ReflectionException;
use \ReflectionMethod as ReflectionMethod;

/**
 * Class Task
 * @package EasyTask
 */
class Task
{
    /**
     * task list
     * @var array
     */
    private $taskList = [];

    /**
     * constructor
     */
    public function __construct()
    {
        //check the operating environment
        Check::analysis();

        //initialize the basic configuration
        $this->setPrefix(Constant::SERVER_PREFIX_VAL);
        $this->setCloseErrorRegister();
        if (UtilHelper::isWin()) {
            Helper::setPhpPath();
            Helper::setCodePage();
        }
    }

    /**
     * setDaemon
     * @param bool $daemon
     * @return $this
     */
    public function setDaemon($daemon = false)
    {
        Env::set(Constant::SERVER_DAEMON_KEY, $daemon);
        return $this;
    }

    /**
     * setPrefix
     * @param string $prefix
     * @return $this
     */
    public function setPrefix($prefix)
    {
        if (Env::get(Constant::SERVER_RUNTIME_PATH)) {
            Helper::showSysError(Constant::SERVER_PREFIX_RUNTIME_PATH_EMPTY_TIP);
        }
        Env::set(Constant::SERVER_PREFIX_KEY, $prefix);
        return $this;
    }

    /**
     * setPhpPath
     * @param string $path
     * @return $this
     */
    public function setPhpPath($path)
    {
        $file = realpath($path);
        if (!file_exists($file)) {
            Helper::showSysError("the path {$path} is not exists");
        }
        Helper::setPhpPath($path);
        return $this;
    }

    /**
     * setTimeZone
     * @param string $timeIdent
     * @return $this
     */
    public function setTimeZone($timeIdent)
    {
        date_default_timezone_set($timeIdent);
        return $this;
    }

    /**
     * setRunTimePath
     * @param string $path
     * @return $this
     */
    public function setRunTimePath($path)
    {
        if (!is_dir($path)) {
            Helper::showSysError("the path {$path} is not exist");
        }
        if (!is_writable($path)) {
            Helper::showSysError("the path {$path} is not writeable");
        }
        Env::set(Constant::SERVER_RUNTIME_PATH, realpath($path));
        return $this;
    }

    /**
     * setAutoRecover
     * @param bool $isRec
     * @return $this
     */
    public function setAutoRecover($isRec = false)
    {
        Env::set(Constant::SERVER_AUTO_RECOVER_KEY, $isRec);
        return $this;
    }

    /**
     * setCloseStdOutLog
     * @param bool $close
     * @return $this
     */
    public function setCloseStdOutLog($close = false)
    {
        Env::set('closeStdOutLog', $close);
        return $this;
    }

    /**
     * setCloseErrorRegister
     * @param bool $close
     * @return $this
     */
    public function setCloseErrorRegister($close = false)
    {
        Env::set(Constant::SERVER_CLOSE_ERROR_REGISTER_SWITCH_KEY, $close);
        return $this;
    }

    /**
     * setErrorRegisterNotify
     * @param string|Closure $notify
     * @return $this
     */
    public function setErrorRegisterNotify($notify)
    {
        if (Env::get(Constant::SERVER_CLOSE_ERROR_REGISTER_SWITCH_KEY)) {
            Helper::showSysError(Constant::SERVER_NOTIFY_MUST_OPEN_ERROR_REGISTER_TIP);
        }
        if (!$notify instanceof Closure && !is_string($notify)) {
            Helper::showSysError(Constant::SERVER_NOTIFY_PARAMS_CHECK_TIP);
        }
        Env::set(Constant::SERVER_NOTIFY_KEY, $notify);
        return $this;
    }

    /**
     * addFunc
     * @param Closure $func
     * @param string $alas
     * @param mixed $time
     * @param int $used
     * @return $this
     * @throws
     */
    public function addFunc($func, $alas, $time = 1, $used = 1)
    {
        $uniqueId = md5($alas);
        if (!($func instanceof Closure)) {
            Helper::showSysError(Constant::SERVER_CHECK_CLOSURE_TYPE_TIP);
        }
        if (isset($this->taskList[$uniqueId])) {
            Helper::showSysError("task $alas already exists");
        }
        Helper::checkTaskTime($time);
        $this->taskList[$uniqueId] = [
            'type' => Constant::SERVER_TASK_FUNC_TYPE,
            'func' => $func,
            'alas' => $alas,
            'time' => $time,
            'used' => $used,
        ];

        return $this;
    }

    /**
     * addClass
     * @param string $class
     * @param string $func
     * @param string $alas
     * @param mixed $time
     * @param int $used
     * @return $this
     * @throws
     */
    public function addClass($class, $func, $alas, $time = 1, $used = 1)
    {
        $uniqueId = md5($alas);
        if (!class_exists($class)) {
            Helper::showSysError("class {$class} is not exist");
        }
        if (isset($this->taskList[$uniqueId])) {
            Helper::showSysError("task $alas already exists");
        }
        try
        {
            $reflect = new ReflectionClass($class);
            if (!$reflect->hasMethod($func)) {
                Helper::showSysError("class {$class}'s func {$func} is not exist");
            }
            $method = new ReflectionMethod($class, $func);
            if (!$method->isPublic()) {
                Helper::showSysError("class {$class}'s func {$func} must public");
            }
            Helper::checkTaskTime($time);
            $this->taskList[$uniqueId] = [
                'type' => $method->isStatic() ? Constant::SERVER_TASK_STATIC_CLASS_TYPE : Constant::SERVER_TASK_OBJECT_CLASS_TYPE,
                'func' => $func,
                'alas' => $alas,
                'time' => $time,
                'used' => $used,
                'class' => $class,
            ];
        } catch (ReflectionException $exception) {
            Helper::showException($exception);
        }

        return $this;
    }

    /**
     * addCommand
     * @param string $command
     * @param string $alas
     * @param mixed $time
     * @param int $used
     * @return $this
     */
    public function addCommand($command, $alas, $time = 1, $used = 1)
    {
        $uniqueId = md5($alas);
        if (!ProcessHelper::canUseExcCommand()) {
            Helper::showSysError(Constant::SERVER_PROCESS_OPEN_CLOSE_DISABLED_TIP);
        }
        if (isset($this->taskList[$uniqueId])) {
            Helper::showSysError(Constant::SERVER_TASK_SAME_NAME_TIP);
        }
        Helper::checkTaskTime($time);
        $this->taskList[$uniqueId] = [
            'type' => Constant::SERVER_TASK_COMMAND_TYPE,
            'alas' => $alas,
            'time' => $time,
            'used' => $used,
            'command' => $command,
        ];

        return $this;
    }

    /**
     * getProcess
     * @return  Win | Linux
     */
    private function getProcess()
    {
        $taskList = $this->taskList;
        return UtilHelper::isWin() ? (new Win($taskList)) : (new Linux($taskList));
    }

    /**
     * start
     * @throws
     */
    public function start()
    {
        //empty task tip
        if (!$this->taskList) {
            Helper::showSysError(Constant::SERVER_TASK_EMPTY_TIP);
        }

        //exception registration
        if (!Env::get(Constant::SERVER_CLOSE_ERROR_REGISTER_SWITCH_KEY)) {
            Error::register();
        }

        //directory construction
        Helper::initAllPath();

        //process start
        $process = $this->getProcess();
        $process->start();
    }

    /**
     * status
     * @throws
     */
    public function status()
    {
        $process = $this->getProcess();
        $process->status();
    }

    /**
     * stop
     * @param bool $force
     * @throws
     */
    public function stop($force = false)
    {
        $process = $this->getProcess();
        $process->stop($force);
    }
}
